// https://github.com/PacktPublishing/Learn-WebAssembly/blob/master/chapter-09-node/testing-example/src/__tests__/main.test.js
const fs = require('fs');

const white = 2; // 0000 0010
const black = 1; // 0000 0001
const crowned_white = 6; // 0000 0110
const crowned_black = 5; // 0000 0101

describe('checkers.wasm tests', () => {
    let checkersInstance;

    beforeAll(async () => {
        const wasmPath = './checkers.wasm';
        const fileBuffer = fs.readFileSync(wasmPath);
        const results = await WebAssembly.instantiate(fileBuffer, {
            env: {
                memoryBase: 0,
                tableBase: 0,
                memory: new WebAssembly.Memory({ initial: 1024 }),
                table: new WebAssembly.Table({ initial: 16, element: 'anyfunc' }),
                abort: console.log
            }
        });
        checkersInstance = results.instance.exports;
    })

    test('Offset for 3, 4 is 140', () => {
        expect(checkersInstance.offsetForPosition(3,4)).toEqual(140);
    });

    test('2 (white) passes isWhite', () => {
        expect(checkersInstance.isWhite(white)).toEqual(1);
    });

    test('1 (black) passes isBlack', () => {
        expect(checkersInstance.isBlack(black)).toEqual(1);
    });

    test('1 (black) does not pass isWhite', () => {
        expect(checkersInstance.isWhite(black)).toEqual(0);
    });

    test('2 (white) does not pass isBlack', () => {
        expect(checkersInstance.isBlack(white)).toEqual(0);
    });

    test('6 (crowned white) passes for white when checked without crown', () => {
        expect(checkersInstance.isWhite(checkersInstance.withoutCrown(crowned_white))).toEqual(1);
    });

    test('5 (crowned black) passes for black when checked without crown', () => {
        expect(checkersInstance.isBlack(checkersInstance.withoutCrown(crowned_black))).toEqual(1);
    });

    test('6 (crowned white) passes for crowned', () => {
        expect(checkersInstance.isCrowned(crowned_white)).toEqual(1);
    });

    test('5 (crowned black) passes for crowned', () => {
        expect(checkersInstance.isCrowned(crowned_black)).toEqual(1);
    });
});
