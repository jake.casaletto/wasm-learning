(module
    (global $BOARD_TILE_WIDTH i32 
        (i32.const 8)
    )
    (global $BYTES_PER_TILE i32 
        (i32.const 4)
    )

    (global $UNOCCUPIED_SPACE i32 
        (i32.const 0) ;; 0000 0000
    )
    (global $BLACK i32
        (i32.const 1) ;; 0000 0001
    )
    (global $WHITE i32
        (i32.const 2) ;; 0000 0010
    )
    (global $CROWN i32
        (i32.const 4) ;; 0000 0100
    )

    (global $CROWNED_BLACK i32
        (i32.const 5) ;; 0000 0101
    )
    (global $CROWNED_WHITE i32
        (i32.const 6) ;; 0000 0110
    )

    (memory $mem 1)

    (func $indexForPosition 
        (param $x i32)
        (param $y i32) 
        (result i32)
            (i32.add
                (i32.mul 
                    (get_global $BOARD_TILE_WIDTH) 
                    (get_local $y) 
                )
                (get_local $x)
            )
    )

    (func $offsetForPosition
        (param $x i32)
        (param $y i32)
        (result i32)
            (i32.mul
                (call $indexForPosition 
                    (get_local $x) 
                    (get_local $y)
                )
                (get_global $BYTES_PER_TILE)
            )
    )

    (func $isCrowned 
        (param $piece i32)
        (result i32)
            (call $isFlagged 
                (get_local $piece) 
                (get_global $CROWN)
            )
    )

    (func $isWhite 
        (param $piece i32)
        (result i32)
            (call $isFlagged 
                (get_local $piece) 
                (get_global $WHITE)
            )
    )

    (func $isBlack 
        (param $piece i32)
        (result i32)
            (call $isFlagged 
                (get_local $piece) 
                (get_global $BLACK)
            )
    )

    (func $withCrown
        (param $piece i32)
        (result i32)
            (i32.or
                (get_local $piece)
                (get_global $CROWN)
            )
    )

    (func $withoutCrown
        (param $piece i32)
        (result i32)
            (i32.or
                (get_local $piece)
                (i32.const 3)
            )
    )

    ;; Determine if a specific flag exists in a bitflags set
    (func $isFlagged 
        (param $bitflags i32)
        (param $flag i32)
        (result i32)
            (i32.eq
                (i32.and 
                    (get_local $bitflags)
                    (get_local $flag)
                )
                (get_local $flag)
            )
    )

    (export "offsetForPosition" 
        (func $offsetForPosition)
    )
    (export "isCrowned" 
        (func $isCrowned)
    )
    (export "withCrown" 
        (func $withCrown)
    )
    (export "withoutCrown" 
        (func $withoutCrown)
    )
    (export "isWhite" 
        (func $isWhite)
    )
    (export "isBlack" 
        (func $isBlack)
    )
)