use crate::errors::{Error, Result};

const DEFAULT_BOARD_WIDTH: usize = 8;
const DEFAULT_JUMP_RANGE: usize = 2;
const DEFAULT_MOVEMENT_RANGE: usize = 1;
const DEFAULT_PIECE_DEPLOYMENT_ROWS: usize = 3;

const MINIMUM_BOARD_SIZE: usize = 6;

const MINIMUM_BOARD_SIZE_MESSAGE: &str = "Board size does not meet the minimum tiles.";
const BOARD_ODD_SIZE: &str = "Board size must be an even number.";
const DEPLOYMENT_ROW_MESSAGE: &str = "Must have at least 1 deployment row for each player.";
const MOVEMENT_MESSAGE: &str = "Movement range must be above 0.";
const JUMP_MESSAGE: &str = "Jump range must be above 0.";

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct BoardConfiguration {
    /// Size of the board in tiles
    /// The board is a square, so height and width of the board will be equal to this
    pub size: usize,
    /// Number of tiles a piece can jump
    pub jump_range: usize,
    /// Number of tiles a piece can move
    pub movement_range: usize,
    /// Number of rows of pieces each player starts with
    pub piece_deployment_rows: usize,
}

impl BoardConfiguration {
    fn from_custom_values(
        size: usize,
        jump_range: usize,
        movement_range: usize,
        piece_deployment_rows: usize,
    ) -> Result<Self> {
        Self::validate(size, jump_range, movement_range, piece_deployment_rows)?;
        Ok(Self {
            size,
            jump_range,
            movement_range,
            piece_deployment_rows,
        })
    }

    fn validate(
        size: usize,
        jump_range: usize,
        movement_range: usize,
        piece_deployment_rows: usize,
    ) -> Result<()> {
        let mut error_strings = vec![];
        if size < MINIMUM_BOARD_SIZE {
            error_strings.push(MINIMUM_BOARD_SIZE_MESSAGE.to_string());
        }
        if size % 2 != 0 {
            error_strings.push(BOARD_ODD_SIZE.to_string());
        }
        if jump_range == 0 {
            error_strings.push(JUMP_MESSAGE.to_string());
        }
        if movement_range == 0 {
            error_strings.push(MOVEMENT_MESSAGE.to_string());
        }
        if piece_deployment_rows < 1 {
            error_strings.push(DEPLOYMENT_ROW_MESSAGE.to_string());
        }

        if error_strings.is_empty() {
            Ok(())
        } else {
            Err(Error::InvalidConfiguration(error_strings.join("  ")))
        }
    }
}

impl Default for BoardConfiguration {
    fn default() -> Self {
        Self {
            size: DEFAULT_BOARD_WIDTH,
            jump_range: DEFAULT_JUMP_RANGE,
            movement_range: DEFAULT_MOVEMENT_RANGE,
            piece_deployment_rows: DEFAULT_PIECE_DEPLOYMENT_ROWS,
        }
    }
}
