use thiserror::Error;

use crate::game_structures::{coordinate::Coordinate, movement::Movement};

pub type Result<T> = core::result::Result<T, Error>;

#[derive(Debug, Clone, PartialEq, Eq, Error)]
pub enum Error {
    #[error("Invalid Game Configuration.  Message: {0}")]
    InvalidConfiguration(String),
    #[error("Game piece not found where expected: {0:?}")]
    GamePieceNotFound(Coordinate),
    #[error("Movement request not a legal move: {0:?}")]
    IllegalMovementRequested(Movement),
}
