use std::ops::Div;

use crate::{
    engine::GameEngine,
    game_structures::{coordinate::Coordinate, game_piece::GamePiece, piece_color::PieceColor},
};

impl GameEngine {
    /// initialize_pieces sets up the pieces in the correct spots on the board
    /// Example, Assuming 8x8 Board:
    ///
    ///    -   0 1 2 3 4 5 6 7
    ///       _________________
    ///    0 |   W   W   W   W
    ///    1 | W   W   W   W
    ///    2 |   W   W   W   W
    ///    3 |
    ///    4 |
    ///    5 | B   B   B   B
    ///    6 |   B   B   B   B
    ///    7 | B   B   B   B
    ///
    ///
    pub fn initialize_pieces(&mut self) {
        self.initialize_player(PieceColor::White);
        self.initialize_player(PieceColor::Black);
    }

    fn initialize_player(&mut self, color: PieceColor) {
        // WHITE DISTRIBUTION:
        // 1. Begins at column 1 for row 0 and alternates between column 1 and column 0 for each row's first piece
        // 2. Distributes a piece every other column

        // BLACK DISTRIBUTION:
        // 1. Begins at column 0 for row ((SIZE-1)-DEPLOYMENT_ROWS) and alternates between column 0 and column 1 for each row's first piece
        // 2. Distributes a piece every other column

        // Since a piece is distributed every other tile, there should be SIZE / 2 pieces per row
        let piece_distribution_per_row = self.configuration.size.div(2);

        let mut piece_distribution_x = Vec::<usize>::new();
        for i in 0..self.configuration.piece_deployment_rows {
            let beginning_column_index = (i + (color.eq(&PieceColor::White) as usize)) % 2;
            piece_distribution_x.append(
                &mut (beginning_column_index..self.configuration.size)
                    .step_by(2)
                    .collect::<Vec<usize>>(),
            );
        }

        let mut piece_distribution_y = Vec::<usize>::new();
        let y_distribution_range = match color {
            PieceColor::White => (0..self.configuration.piece_deployment_rows),
            PieceColor::Black => {
                (self.configuration.size - self.configuration.piece_deployment_rows)
                    ..self.configuration.size
            }
        };
        for i in y_distribution_range {
            // https://stackoverflow.com/questions/59413614/cycle-a-rust-iterator-a-given-number-of-times
            let row = vec![i];
            let mut row = std::iter::repeat(row.iter())
                .take(piece_distribution_per_row)
                .flatten()
                .map(|x| *x)
                .collect::<Vec<usize>>();
            piece_distribution_y.append(&mut row);
        }

        piece_distribution_x
            .iter()
            .zip(piece_distribution_y.iter())
            .map(|(a, b)| (*a as usize, *b as usize))
            .for_each(|(x, y)| {
                self.board_pieces
                    .insert(Coordinate(x, y), GamePiece::new(color));
            });
    }
}
