use crate::{
    engine::GameEngine,
    errors::{Error, Result},
    game_structures::{
        coordinate::Coordinate, game_piece::GamePiece, movement::Movement,
        movement_result::MovementResult, piece_color::PieceColor,
    },
};
use std::ops::AddAssign;

impl GameEngine {
    pub fn advance_turn(&mut self) {
        self.move_count.add_assign(1);
        self.current_turn = match self.current_turn {
            PieceColor::Black => PieceColor::White,
            PieceColor::White => PieceColor::Black,
        };
    }

    pub fn move_piece(&mut self, movement: &Movement) -> Result<MovementResult> {
        let legal_moves = self.legal_moves();

        if !legal_moves.contains(movement) {
            return Err(Error::IllegalMovementRequested(movement.clone()));
        }

        let Coordinate(fx, fy) = movement.from;
        let Coordinate(tx, ty) = movement.to;
        let piece = self.board_piece(&Coordinate(fx, fy))?;
        let midpiece_coordinate = self.midpiece_coordinate(fx, fy, tx, ty);
        if let Some(Coordinate(x, y)) = midpiece_coordinate {
            self.board_pieces.remove(&Coordinate(x, y)); // Remove the jumped piece
        }

        // Move piece from source to dest
        self.board_pieces.insert(Coordinate(tx, ty), piece.clone());
        self.board_pieces.remove(&Coordinate(fx, fy));

        let crowned = if self.should_crown(&piece, &movement.to) {
            self.crown_piece(&movement.to);
            true
        } else {
            false
        };

        self.advance_turn();

        Ok(MovementResult {
            movement: movement.clone(),
            crowned,
        })
    }

    pub fn legal_moves(&self) -> Vec<Movement> {
        let mut moves: Vec<Movement> = Vec::new();
        for col in 0..self.configuration.size {
            for row in 0..self.configuration.size {
                if let Some(&piece) = self.board_pieces.get(&Coordinate(col, row)) {
                    if piece.color == self.current_turn {
                        let loc = Coordinate(col, row);
                        let mut vmoves = self.valid_moves_from(loc);
                        moves.append(&mut vmoves);
                    }
                }
            }
        }
        moves
    }

    fn valid_moves_from(&self, loc: Coordinate) -> Vec<Movement> {
        let Coordinate(x, y) = loc;
        if let Some(p) = self.board_pieces.get(&Coordinate(x, y)) {
            let mut jumps = loc
                .jump_targets_from(Some(self.configuration))
                .filter(|t| self.valid_jump(&p, &loc, &t))
                .map(|ref t| Movement {
                    from: loc.clone(),
                    to: t.clone(),
                })
                .collect::<Vec<Movement>>();
            let mut moves = loc
                .move_targets_from(Some(self.configuration))
                .filter(|t| self.valid_move(&p, &loc, &t))
                .map(|ref t| Movement {
                    from: loc.clone(),
                    to: t.clone(),
                })
                .collect::<Vec<Movement>>();
            jumps.append(&mut moves);
            jumps
        } else {
            Vec::new()
        }
    }

    pub fn should_crown(&self, piece: &GamePiece, piece_location: &Coordinate) -> bool {
        todo!()
    }
    pub fn crown_piece(&mut self, piece_location: &Coordinate) {
        todo!()
    }

    pub fn valid_move(
        &self,
        piece: &GamePiece,
        piece_location: &Coordinate,
        target: &Coordinate,
    ) -> bool {
        todo!()
    }

    pub fn valid_jump(
        &self,
        piece: &GamePiece,
        piece_location: &Coordinate,
        target: &Coordinate,
    ) -> bool {
        todo!()
    }

    pub fn midpiece_coordinate(
        &self,
        fx: usize,
        fy: usize,
        tx: usize,
        ty: usize,
    ) -> Option<Coordinate> {
        todo!()
    }

    fn board_piece(&self, coordinate: &Coordinate) -> Result<GamePiece> {
        let result = self.board_pieces.get(&coordinate);
        if let Some(piece) = result {
            Ok(piece.clone())
        } else {
            Err(Error::GamePieceNotFound(coordinate.clone()))
        }
    }
}
