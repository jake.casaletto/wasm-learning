use crate::game_structures::movement::Movement;

pub struct MovementResult {
    pub movement: Movement,
    pub crowned: bool,
}
