use crate::game_structures::coordinate::Coordinate;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Movement {
    pub from: Coordinate,
    pub to: Coordinate,
}
impl Movement {
    pub fn new(from: (usize, usize), to: (usize, usize)) -> Self {
        Self {
            from: Coordinate(from.0, from.1),
            to: Coordinate(to.0, to.1),
        }
    }
}
