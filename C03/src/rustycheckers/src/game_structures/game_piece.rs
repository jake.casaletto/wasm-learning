use crate::game_structures::piece_color::PieceColor;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct GamePiece {
    pub color: PieceColor,
    pub crowned: bool,
}
impl GamePiece {
    pub fn new(color: PieceColor) -> Self {
        Self {
            color,
            crowned: false,
        }
    }

    pub fn crown_piece(piece: Self) -> Self {
        Self {
            color: piece.color,
            crowned: true,
        }
    }
}
