#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum PieceColor {
    White,
    Black,
}
