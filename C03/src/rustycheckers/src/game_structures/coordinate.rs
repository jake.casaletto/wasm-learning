use crate::configuration::BoardConfiguration;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct Coordinate(pub usize, pub usize);
impl Coordinate {
    pub fn on_board(self, configuration: Option<BoardConfiguration>) -> bool {
        let Coordinate(x, y) = self;
        let configuration = configuration.unwrap_or_default();
        x < configuration.size && y < configuration.size
    }

    pub fn jump_targets_from(
        &self,
        configuration: Option<BoardConfiguration>,
    ) -> impl Iterator<Item = Coordinate> {
        let mut jumps = Vec::new();
        let Coordinate(x, y) = *self;
        let configuration = configuration.unwrap_or_default();

        if y > 1 {
            jumps.push(Coordinate(
                x + configuration.jump_range,
                y - configuration.jump_range,
            ));
        }
        jumps.push(Coordinate(
            x + configuration.jump_range,
            y + configuration.jump_range,
        ));

        if x > 1 {
            jumps.push(Coordinate(
                x - configuration.jump_range,
                y + configuration.jump_range,
            ));
        }

        if x > 1 && y > 1 {
            jumps.push(Coordinate(
                x - configuration.jump_range,
                y - configuration.jump_range,
            ));
        }

        jumps.into_iter()
    }

    pub fn move_targets_from(
        &self,
        configuration: Option<BoardConfiguration>,
    ) -> impl Iterator<Item = Coordinate> {
        let mut moves = Vec::new();
        let Coordinate(x, y) = *self;
        let configuration = configuration.unwrap_or_default();

        if y > 0 {
            moves.push(Coordinate(
                x + configuration.movement_range,
                y - configuration.movement_range,
            ));
        }
        moves.push(Coordinate(
            x + configuration.movement_range,
            y + configuration.movement_range,
        ));

        if x > 0 {
            moves.push(Coordinate(
                x - configuration.movement_range,
                y + configuration.movement_range,
            ));
        }

        if x > 0 && y > 0 {
            moves.push(Coordinate(
                x - configuration.movement_range,
                y - configuration.movement_range,
            ));
        }
        moves.into_iter()
    }
}
