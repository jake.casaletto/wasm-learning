use std::collections::HashMap;
use std::ops::{AddAssign, Div, Mul};

use crate::{
    configuration::BoardConfiguration,
    errors::{Error, Result},
    game_structures::{
        coordinate::Coordinate, game_piece::GamePiece, movement::Movement,
        movement_result::MovementResult, piece_color::PieceColor,
    },
};

mod initialization;
mod player_turn;

pub struct GameEngine {
    configuration: BoardConfiguration,
    board_pieces: HashMap<Coordinate, GamePiece>,
    current_turn: PieceColor,
    move_count: u32,
}

impl GameEngine {
    pub fn new(configuration: Option<BoardConfiguration>) -> Self {
        let configuration = configuration.unwrap_or_default();
        let board = HashMap::with_capacity(
            configuration
                .piece_deployment_rows
                .mul(configuration.size.div(2)),
        );
        let mut engine = Self {
            configuration,
            board_pieces: board,
            current_turn: PieceColor::Black,
            move_count: 0,
        };
        engine.initialize_pieces();
        engine
    }
}
